import Vue from 'vue';
import jwtDecode from 'jwt-decode';

import App from './App.vue';
import router from './router';
import store from './store';
import { DecodedAccessToken } from '@/types';

// Собственные плагины
import BaseComponents from './plugins/BaseComponents';
import Notifications from './plugins/Notifications';

// Сторонние плагины
import './setup/veeValidate';

// Глобальные стили
import './assets/styles/base.scss';

Vue.use(BaseComponents);
Vue.use(Notifications);

Vue.config.productionTip = false;

new Vue({
  router,
  store,
  render: h => h(App),
  created() {
    const accessToken = localStorage.getItem('accessToken');
    if (accessToken) {
      const decodedToken: DecodedAccessToken = jwtDecode(accessToken);
      this.$store.dispatch('profile/fetchUser', decodedToken.uid);
    }
  },
}).$mount('#app');
