import { extend } from 'vee-validate';
import {
  required,
  email,
  max,
  alpha,
} from 'vee-validate/dist/rules';
import ruLocale from 'vee-validate/dist/locale/ru.json';

const usingRules = {
  required,
  email,
  max,
  alpha,
};
const ruMessages = (<any>ruLocale).messages;

// eslint-disable-next-line
for (const [rule, validation] of Object.entries(usingRules)) {
  extend(rule, {
    ...validation,
    message: ruMessages[rule],
  });
}
