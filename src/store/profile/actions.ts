import { ActionTree } from 'vuex';

import { ProfileState, User } from './types';
import { RootState } from '../types';

import { Users as UsersService } from '@/services';
import { ProfileFormData } from '@/types';

const actions: ActionTree<ProfileState, RootState> = {
  async fetchUser({ commit }, id: number): Promise<void> {
    const response = await UsersService.show(id);
    const user: User = response && response.data.user;

    commit('setUser', user);
  },
  async updateUser({ commit }, { id, data }: { id: number; data: ProfileFormData }): Promise<void> {
    const response = await UsersService.update(id, data);
    const user: User = response && response.data.user;

    commit('setUser', user);
  },
};

export default actions;
