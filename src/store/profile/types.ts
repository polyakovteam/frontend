export interface User {
  id: number;
  email: string;
  password: string;
  username: string;
  firstName: string | null;
  lastName: string | null;
}

export interface ProfileState {
  user: User | null;
}
