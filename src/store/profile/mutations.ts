import { MutationTree } from 'vuex';

import { ProfileState, User } from './types';

const mutations: MutationTree<ProfileState> = {
  setUser(state, payload: User) {
    state.user = payload;
  },
};

export default mutations;
