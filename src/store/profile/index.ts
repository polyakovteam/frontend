import { Module } from 'vuex';

import mutations from './mutations';
import actions from './actions';

import { ProfileState } from './types';
import { RootState } from '../types';

export const state: ProfileState = {
  user: null,
};

export const profile: Module<ProfileState, RootState> = {
  namespaced: true,
  state,
  mutations,
  actions,
};
