import Vue from 'vue';
import VueRouter from 'vue-router';
import Home from '../views/Home.vue';

Vue.use(VueRouter);

const routes = [
  {
    path: '/',
    name: 'home',
    component: Home,
  },
  {
    path: '/auth',
    name: 'auth',
    redirect: { name: 'signIn' },
    component: () => import(/* webpackChunkName: "auth" */ '../views/EmptyView.vue'),
    children: [
      {
        path: 'sign-in',
        name: 'signIn',
        component: () => import(/* webpackChunkName: "auth" */ '../views/auth/SignIn.vue'),
      },
      {
        path: 'registration',
        name: 'registration',
        component: () => import(/* webpackChunkName: "auth" */ '../views/auth/Registration.vue'),
      },
    ],
  },
  {
    path: '/profile',
    name: 'profile',
    component: () => import(/* webpackChunkName: "profile" */ '../views/Profile.vue'),
  },
];

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes,
});

export default router;
