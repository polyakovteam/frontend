import eventBus from './eventBus';
import NotificationList from './components/NotificationList.vue';

export default {
  install(Vue: any) {
    Vue.component('app-notifications', NotificationList);

    Vue.prototype.$notify = (params: any) => {
      eventBus.$emit('add', params);
    };
  },
};
