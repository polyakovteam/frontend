import AppAvatar from '@/components/base/AppAvatar.vue';
import AppContentWrapper from '@/components/base/AppContentWrapper.vue';
import AppPopup from '@/components/base/AppPopup.vue';

// Контролы
import AppButton from '@/components/base/controls/AppButton.vue';
import AppField from '@/components/base/controls/AppField.vue';
import AppInput from '@/components/base/controls/AppInput.vue';
import AppInputError from '@/components/base/controls/AppInputError.vue';
import AppLink from '@/components/base/controls/AppLink.vue';

export default {
  install(Vue: any) {
    Vue.component('AppAvatar', AppAvatar);
    Vue.component('AppContentWrapper', AppContentWrapper);
    Vue.component('AppPopup', AppPopup);

    // Контролы
    Vue.component('AppButton', AppButton);
    Vue.component('AppField', AppField);
    Vue.component('AppInput', AppInput);
    Vue.component('AppInputError', AppInputError);
    Vue.component('AppLink', AppLink);
  },
};
