export interface DecodedAccessToken {
  uid: number;
  iat: number;
}

export interface NewUser {
  email: string;
  password: string;
  username: string;
}

export interface SignInFormData {
  email: string;
  password: string;
}

export interface AuthUser {
  email: string;
  username: string;
  firstName: string | null;
  lastName: string | null;
}

export interface ProfileFormData {
  email: string;
  username: string;
  firstName: string | null;
  lastName: string | null;
}
