import Service from './Service';

export default class AuthService extends Service {
  register(payload: object) {
    return this.apiClient.post('register', payload);
  }

  login(payload: object) {
    return this.apiClient.post('login', payload);
  }
}
