import AuthService from './AuthService';
import CommonService from './CommonService';

export const Auth = new AuthService({ path: 'auth' });
export const Users = new CommonService({ path: 'users' });
