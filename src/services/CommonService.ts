import Service from './Service';

export default class CommonService extends Service {
  show(id: number) {
    return this.apiClient.get(`/${id}`);
  }

  update(id: number, data: object) {
    return this.apiClient.patch(`/${id}`, data);
  }
}
