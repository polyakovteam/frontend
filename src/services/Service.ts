import axios from 'axios';

const backendUrl = process.env.VUE_APP_BACKEND_URL;

export default class Service {
  path: string;

  apiClient: any;

  constructor({ version = 'v1', path = '' }) {
    this.path = path;
    this.apiClient = axios.create({
      baseURL: `${backendUrl}/api/${version}/${path}`,
      withCredentials: false,
    });

    this.apiClient.interceptors.request.use((config: any) => {
      const accessToken = localStorage.getItem('accessToken');
      if (accessToken) {
        config.headers.Authorization = `Bearer ${accessToken}`;
      }
      return config;
    }, (error: any) => Promise.reject(error));

    this.apiClient.interceptors.response.use((response: any) => {
      const { accessToken } = response.data;

      if (accessToken) {
        localStorage.setItem('accessToken', accessToken.token);
      }
      return response;
    }, (error: any) => {
      if (error.response && error.response.status === 401) {
        localStorage.removeItem('accessToken');
      }
      return Promise.reject(error);
    });
  }
}
